#!/bin/bash
set -e

docker rm $(docker ps -a -q) --force
docker network rm mongo-shard