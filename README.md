
# MongoShards on multiple dockers

Based on the article of [Gustavo Leitão](https://medium.com/@gustavo.leitao/criando-um-cluster-mongodb-com-replicaset-e-sharding-com-docker-9cb19d456b56) this scritpt create a MongoDb cluester with replicaset and shard.

This script was testesd on a Mac but should work on Linux also.

To create the cluster execute the script:

```sh
sh ./orchestractor.sh
```

To shutsown the cluster execute the script:

```sh
sh ./clean.sh
```